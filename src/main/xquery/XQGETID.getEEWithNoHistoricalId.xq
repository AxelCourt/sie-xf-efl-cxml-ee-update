xquery version "1.0-ml";

declare namespace xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst";

declare variable $EE.COLLECTION as xs:string external;
declare variable $EE.TEE as xs:string external;

let $eeList := collection($EE.COLLECTION)/xf:editorialEntity[@code = $EE.TEE][not(@xf:historicalId)]
return
  <xf:editorialEntityList count="{count($eeList)}">
  {
    for $ee in $eeList
    return
      <xf:editorialEntity xf:id="{$ee/@xf:id}"/>
  }
  </xf:editorialEntityList>
