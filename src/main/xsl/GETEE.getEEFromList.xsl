<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns:eeUp="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/ee-updater"
                xmlns="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
                xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
                exclude-result-prefixes="#all"
                version="3.0">
  
  
  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Récupère des EE via l'API de sérialisation XML de FLASH.</xd:p>
      <xd:p>Input : liste d'ID FLASH d'EE à corriger, sous la forme : &lt;xf:editorialEntity xf:id="ff978dad0-ec5c-406b-b265-bb143cd0bcca"/&gt;</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!-- IMPORTS -->
  <xsl:import href="xslLib:/xslLib/els-log.xsl"/>
  
  <!-- URI du répertoire de copie des sérialisations des EE -->
  <xsl:param name="EE.SERIALIZED.OUTPUT.DIR.URI" select="''" as="xs:string"/>
  <!-- Serveur ECM, pour appel au WS de sérialisation des EE -->
  <xsl:param name="ECM.SERVER.URL" select="''" as="xs:string"/>
  <!-- URL du WS de sérialisation sans l'ID de l'EE -->
  <xsl:variable name="eeUrlBegin" select="concat($ECM.SERVER.URL,'ecm-core/api/xmlExporter/exportToXML?flashId=')" as="xs:string"/>
  <!-- Pour log -->
  <xsl:variable name="els:currentResName" select="static-base-uri()" as="xs:string"/>
  
  <!-- Modes -->
  <xsl:mode name="eeUp:getEEFromList" on-no-match="shallow-copy"/>
  
  <xd:doc>
    <xd:desc>Match par défaut.</xd:desc>
  </xd:doc>
  <xsl:template match="/ | /*">
    <xsl:apply-templates select="." mode="eeUp:getEEFromList"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | MODE eeUp:getEEFromList                          |
      +==================================================+-->
  
  <xd:doc>
    <xd:desc>Récupération de la sérialisation XML d'une EE dans l'ECM à partir de son FLASH ID.</xd:desc>
  </xd:doc>
  <xsl:template match="xf:editorialEntity" mode="eeUp:getEEFromList">
    <!-- Tentative de récupération de l'EE -->
    <xsl:variable name="ee.uri" select="concat($eeUrlBegin,@xf:id)" as="xs:string"/>
    <xsl:variable name="ee" as="document-node()?">
      <xsl:if test="doc-available($ee.uri)">
        <xsl:sequence select="doc($ee.uri)"/>
      </xsl:if>
    </xsl:variable>
    <!-- Si EE récupérée -->
    <xsl:if test="not(empty($ee))">
      <!-- Log -->
      <xsl:call-template name="els:log">
        <xsl:with-param name="xsltName" select="$els:currentResName" as="xs:string"/>
        <xsl:with-param name="level" select="'info'" as="xs:string"/>
        <xsl:with-param name="code" select="'ECM.EE.FOUND'" as="xs:string"/>
        <xsl:with-param name="markup" select="false()"/>
        <xsl:with-param name="description" as="xs:string*">EE trouvée : <xsl:value-of select="@xf:id"/>.</xsl:with-param>
      </xsl:call-template>
      <!-- Dump de la sérialisation après patch -->
      <xsl:result-document format="els:xml" href="{resolve-uri(concat(@xf:id,'.xml'),$EE.SERIALIZED.OUTPUT.DIR.URI)}">
        <xsl:sequence select="$ee"/>
      </xsl:result-document>
    </xsl:if>
    <!-- Copie du noeud courant pour l'output principal -->
    <xsl:copy copy-namespaces="no">
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:attribute name="eeWasFound" select="not(empty($ee))"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>