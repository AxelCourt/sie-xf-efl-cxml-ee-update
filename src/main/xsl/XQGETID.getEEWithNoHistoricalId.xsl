<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns:eeUp="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/ee-updater"
                xmlns:mkl-ext="fr:askjadev:xml:extfunctions"
                xmlns="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
                xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
                exclude-result-prefixes="#all"
                version="3.0">
  
  
  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Joue une XQuery dans MarkLogic pour récupérer une liste d'EE sans historicalId appartenant à la collection spécifiée.</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!-- IMPORTS -->
  <xsl:import href="xslLib:/xslLib/els-log.xsl"/>
  
  <!-- Paramètres de l'instance MarkLogic -->
  <xsl:param name="MKL.SERVER" required="yes" as="xs:string"/>
  <xsl:param name="MKL.PORT" required="yes" as="xs:string"/>
  <xsl:param name="MKL.USER" required="yes" as="xs:string"/>
  <xsl:param name="MKL.PASSWORD" required="yes" as="xs:string"/>
  <!-- Variables globales de la XQuery -->
  <xsl:param name="EE.COLLECTION" required="yes" as="xs:string"/>
  <xsl:param name="EE.TEE" required="yes" as="xs:string"/>
  
  <!-- Pour log -->
  <xsl:variable name="els:currentResName" select="static-base-uri()" as="xs:string"/>
    
  <xd:doc>
    <xd:desc>Match par défaut.</xd:desc>
  </xd:doc>
  <xsl:template match="/ | /*">
    <xsl:apply-templates select="." mode="eeUp:getEEWithNoHistoricalId"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | MODE eeUp:getEEFromList                          |
      +==================================================+-->
  
  <xd:doc>
    <xd:desc>
      <xd:p>Lancement de la XQuery, récupération du résultat, log en cas de résultat vide.</xd:p>
      <xd:p>L'input source n'a pas d'importance, il n'est pas restitué.</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template match="/ | /*" mode="eeUp:getEEWithNoHistoricalId">
    <!-- Fichier XQuery à exécuter -->
    <xsl:variable name="xqueryUri" select="resolve-uri('../xquery/XQGETID.getEEWithNoHistoricalId.xq',static-base-uri()) cast as xs:string" as="xs:string"/>
    <!-- Paramètres de connexion à MarkLogic -->
    <xsl:variable name="mklConf"
                  as="map(xs:string,xs:anyAtomicType?)"
                  select="map{
                            'server'    : $MKL.SERVER,
                            'port'      : $MKL.PORT cast as xs:integer,
                            'user'      : $MKL.USER,
                            'password'  : $MKL.PASSWORD
                          }"/>
    <!-- Variables globales de la XQuery -->
    <xsl:variable name="mapVar"
                  as="map(xs:QName,item())"
                  select="map{
                            xs:QName('EE.COLLECTION') : $EE.COLLECTION,
                            xs:QName('EE.TEE')        : $EE.TEE
                  }"/>
    <!-- Exécution de la XQuery -> appel à la fonction d'extension mkl-ext:marklogic-query() -->
    <xsl:variable name="xqResult" as="element(xf:editorialEntityList)?">
      <xsl:sequence select="mkl-ext:marklogic-query-uri($xqueryUri,$mklConf,$mapVar)"/>
    </xsl:variable>
    <!-- Si des EE sans historicalId ont été trouvées, la XQuery renvoie une liste d'EE sous la forme :
           <xf:editorialEntityList count="xxx">
             <xf:editorialEntity xf:id="yyy"/>
             ...
           </xf:editorialEntityList> -->
    <xsl:if test="$xqResult/*/@count = '0'">
      <xsl:call-template name="els:log">
        <xsl:with-param name="xsltName" select="$els:currentResName" as="xs:string"/>
        <xsl:with-param name="level" select="'warning'" as="xs:string"/>
        <xsl:with-param name="code" select="'MKL.NO.EE.FOUND'" as="xs:string"/>
        <xsl:with-param name="markup" select="false()"/>
        <xsl:with-param name="description" as="xs:string*">Aucune EE sans historicalId n'a été trouvée dans la collection MarkLogic "<xsl:value-of select="$EE.COLLECTION"/>" pour le TEE "<xsl:value-of select="$EE.TEE"/>".</xsl:with-param>
      </xsl:call-template>
    </xsl:if>
    <!-- Ouput -->
    <xsl:sequence select="$xqResult"/>
  </xsl:template>
  
</xsl:stylesheet>