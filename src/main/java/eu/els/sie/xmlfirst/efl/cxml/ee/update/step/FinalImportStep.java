package eu.els.sie.xmlfirst.efl.cxml.ee.update.step;

import eu.els.sie.xmlfirst.efl.cxml.ee.update.EEUpdaterException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmSequenceIterator;
import net.sf.saxon.s9api.XdmValue;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.AllFileSelector;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class to manage the final (re)import after patching the EE
 * @author ext-acourt
 */
public class FinalImportStep {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(FinalImportStep.class);
            
    /**
     * Callable method to create a zip archive from a directory and send it to an XD listener directory, so that its content can be imported to FLASH.
     * @param value A XdmValue sequence containing 3 strings corresponding to : the URI of the directory which content must be zipped, the URI of the ZIP file to create, and the URI of the distant directory (sftp://login@pass:host/path).
     * @return Boolean true if the zip and distant copy went well.
     * @throws EEUpdaterException
     * @throws URISyntaxException
     * @throws java.net.MalformedURLException
     * @throws org.apache.commons.vfs2.FileSystemException
     * @throws IOException
     */
    public static XdmValue createZipAndSendItToXD(XdmValue value) throws EEUpdaterException, URISyntaxException, MalformedURLException, FileSystemException, IOException {
        // Get the arguments
        // value is a Xdm sequence containing 3 string values :
        //   - URI of the directory to compress
        //   - URI of the ZIP file that will be created
        //   - distant URI of the directory where the ZIP must be sent
        if (value.size() != 3) {
            throw new EEUpdaterException("Method createZipAndSendItToXD of class FinalImportStep must be called with a sequence containing: directoryURI as xs:string, zipURI as xs:string and distantURI as xs:string.");
        }
        XdmSequenceIterator it = value.iterator();
        String directoryUri = it.next().getStringValue();
        String zipUri = it.next().getStringValue();
        String procUri = FilenameUtils.getPath(zipUri) + FilenameUtils.getBaseName(zipUri) + ".proc";
        String distantUri = it.next().getStringValue();      
        LOGGER.info("Invoking step FinalImportStep#createZipAndSendItToXD with arguments: directoryUri={}, zipUri={}, distantUri={}", directoryUri, zipUri, distantUri);
        // Make the zip
        makeZipFile(directoryUri, zipUri);
        // Make the XD .proc file
        makeProcFile(procUri);
        // Send data to XD
        sendToXD(zipUri, procUri, distantUri);
        // Return true if no exception was thrown
        return new XdmAtomicValue(true);
    }

    /**
     * Private method to compress a directory content into a ZIP file.
     */
    private static void makeZipFile(String directoryUri, String zipUri) throws URISyntaxException, MalformedURLException, IOException {
        ZipOutputStream zipFile = new ZipOutputStream(FileUtils.openOutputStream(FileUtils.toFile(new URI(zipUri).toURL())));        
        for (File file : FileUtils.toFile(new URI(directoryUri).toURL()).listFiles()) {
            ZipEntry entry = new ZipEntry(file.getName());
            zipFile.putNextEntry(entry);
            FileInputStream in = FileUtils.openInputStream(file);
            IOUtils.copy(in, zipFile);
            in.close();
        }
        zipFile.flush();
        zipFile.close();
    }

    /**
     * Private method to create a .proc file which will be sent to XD with the ZIP file.
     */
    private static void makeProcFile(String procUri) throws URISyntaxException, MalformedURLException, IOException {
        File procFile = FileUtils.toFile(new URI(procUri).toURL());
        FileUtils.writeStringToFile(procFile, "OK", "UTF-8");
    }

    /**
     * Private method to send the ZIP file + the .proc file to a XD listener directory.
     */
    private static void sendToXD(String zipUri, String procUri, String distantUri) throws FileSystemException {
        StandardFileSystemManager manager = new StandardFileSystemManager();
        FileSystemOptions opts = new FileSystemOptions();
        SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, false);
        SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, 10000);
        manager.init();
        SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, "no");
        // First, send the ZIP
        FileObject fromZip = manager.resolveFile(zipUri, opts);
        FileObject toZip = manager.resolveFile(distantUri + "/" + FilenameUtils.getName(zipUri), opts);
        toZip.copyFrom(fromZip, new AllFileSelector());
        // Then, send the .proc file
        FileObject fromProc = manager.resolveFile(procUri, opts);
        FileObject toProc = manager.resolveFile(distantUri + "/" + FilenameUtils.getName(procUri), opts);
        toProc.copyFrom(fromProc, new AllFileSelector());
        manager.close();
    }
    
}
