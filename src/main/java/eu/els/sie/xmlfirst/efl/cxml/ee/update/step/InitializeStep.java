package eu.els.sie.xmlfirst.efl.cxml.ee.update.step;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmValue;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class to initialize an input document
 * @author ext-acourt
 */
public class InitializeStep {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(InitializeStep.class);

    /**
     * Create a blank XML document that can be used as the input of a Gaulois-Pipe program.
     * This is useful when the first step of the Gaulois-Pipe performs an action that does not require to parse or transform a specific XML document.
     * Ex.: Executing an XQuery to get information that will then be used by another XSL.
     * @param value A XdmValue sequence containing only one value, that is the document URI supplied as a string.
     * @return Boolean true if the document creation did not raise any exception.
     * @throws URISyntaxException
     * @throws MalformedURLException
     * @throws IOException
     * @throws XMLStreamException
     */
    public static XdmValue createInputDocument(XdmValue value) throws URISyntaxException, MalformedURLException, IOException, XMLStreamException {
        // Get the output file
        String outputUri = value.itemAt(0).getStringValue();
        LOGGER.info("Invoking step InitializeStep#createInputDocument with arguments: outputUri={}", outputUri);
        File outputFile = FileUtils.toFile(new URI(outputUri).toURL());
        // Create the document
        // Only a document element is needed
        XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
        XMLStreamWriter xsw = outputFactory.createXMLStreamWriter(FileUtils.openOutputStream(outputFile),"UTF-8");
        xsw.writeStartDocument("UTF-8", "1.0");
        xsw.writeStartElement("_BLANK");
        xsw.writeEndElement();
        xsw.writeEndDocument();
        xsw.flush();
        xsw.close();
        // Return true if no exception was thrown
        return new XdmAtomicValue(true);
    }

}
