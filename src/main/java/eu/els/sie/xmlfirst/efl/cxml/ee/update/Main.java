package eu.els.sie.xmlfirst.efl.cxml.ee.update;

import com.processing.utils.Param;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main executable class.
 * @author ext-acourt
 */
public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    
    /**
     * @param args The command line arguments
     */
    public static void main(String[] args) {
        LOGGER.info("APPLICATION START - Command line arguments: {}", Arrays.asList(args).toString());
        boolean runStatus = false;
        try {
            EEUpdater eeUp = new EEUpdater(args[0], args[1]);
            if (args.length == 3) {
                // Overrides GP / configuration defined working directory
                eeUp.getGppParameters().put("WORKING.DIR.URI", new Param("WORKING.DIR.URI", args[2]));
            }
            runStatus = eeUp.run();
        }
        catch (EEUpdaterException ex) {
            LOGGER.error("APPLICATION STOP - An exception was raised.", ex);
        }
        if (runStatus) {
            LOGGER.info("APPLICATION END - Execution was a success.");
        }
        else {
            LOGGER.error("APPLICATION END - Execution encountered an error: see the logs for more information.");
        }
    }
    
}
