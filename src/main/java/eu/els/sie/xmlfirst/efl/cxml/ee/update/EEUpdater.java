package eu.els.sie.xmlfirst.efl.cxml.ee.update;

import com.processing.exception.ProcessingException;
import com.processing.exception.XPathEvaluationException;
import com.processing.step.ProcessingStep;
import com.processing.utils.EXpath;
import com.processing.utils.Param;
import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import net.sf.saxon.Configuration;
import net.sf.saxon.lib.StandardUnparsedTextResolver;
import net.sf.saxon.trans.XPathException;
import top.marchand.xml.gaulois.impl.DefaultSaxonConfigurationFactory;

/**
 * A class to create, load and launch a Gaulois-Pipe ProcessingStep.
 * @author ext-acourt
 */
public class EEUpdater {
    
    private Map<String, Param> gppParams = new HashMap<>();
    private ProcessingStep gpp;
    private Configuration saxonConfig;
    
    /**
     * Default constructor.
     * Create a blank EEUpdated object, with no associated Gaulois-Pipe ProcessingStep.
     */
    public EEUpdater() { }
    
    /**
     * Create an EEUpdater object with a default Galois-Pipe ProcessingStep, then load the Gaulois-Pipe Processing file and creates a map for parameters with the given parameters file URI.
     * @param gppFileUri URI of the Gaulois-Pipe Processing file.
     * @param gppParamsFileUri URI of the Gaulois-Pipe Processing parameters file.
     * @throws EEUpdaterException If an error is encountered while loading the GPP file ou GPP parameters file.
     */
    public EEUpdater(String gppFileUri, String gppParamsFileUri) throws EEUpdaterException {
        // Create a default Gaulois-Pipe ProcessingStep
        createGppInstanceFromDefaultConfig();
        // Try to load the Gaulois-Pipe Processing file
        try {
            loadGppFile(gppFileUri);
        }
        catch (ProcessingException ex) {
            throw new EEUpdaterException("Error while loading the Gaulois-Pipe Processing file.", ex);
        }
        // Try to load the Gaulois-Pipe Processing parameters file
        try {
            readGppParametersFromFile(gppParamsFileUri);
        }
        catch (IOException | URISyntaxException | XPathException | XPathEvaluationException ex) {
            throw new EEUpdaterException("Error while loading the Gaulois-Pipe Processing parameters file.", ex);
        }
    }
    
    /**
     * Run the ProcessingStep instance with the registered Processing file and Processing parameters.
     * @return true/false according if the execution of the ProcessingStep was a success.
     * @throws EEUpdaterException If an error occurred during the execution or if the Processing was not correctly instantiated.
     */
    public boolean run() throws EEUpdaterException {
        if (gpp != null) {
            if (gpp.getUri() != null) {
                try {
                    return gpp.execute(gppParams);
                }
                catch (ProcessingException ex) {
                    throw new EEUpdaterException("An error occurred while running the Gaulois-Pipe Processing file.", ex);
                }
            }
            else {
                throw new EEUpdaterException("A Processing file must be loaded before executing anything.");
            }
        }
        else {
            throw new EEUpdaterException("An instance of ProcessingStep must have been created before executing anything.");
        }
    }
    
    /**
     * Create a Gaulois-Pipe ProcessingStep instance with a default Gaulois-Pipe Configuration.
     */
    public final void createGppInstanceFromDefaultConfig() {
        saxonConfig = new DefaultSaxonConfigurationFactory().getConfiguration();
        setGppInstance(new ProcessingStep(saxonConfig));
    }

    /**
     * Create a Gaulois-Pipe ProcessingStep instance with the supplied Saxon Configuration.
     * @param saxonConfig An existing Saxon Configuration.
     */
    public void createGppInstanceFromConfig(Configuration saxonConfig) {
        this.saxonConfig = saxonConfig;
        setGppInstance(new ProcessingStep(saxonConfig));
    }
    
    /**
     * Set the Gaulois-Pipe ProcessingStep instance.
     * @param gpp A Gaulois-Pipe ProcessingStep.
     */
    public final void setGppInstance(ProcessingStep gpp) {
        this.gpp = gpp;
    }
    
    /**
     * Read a set of parameters from a properties file and add them to the Gaulois-Pipe ProcessingStep instance.
     * @param fileUri URI of the file from which the properties must be read from.
     * @throws java.io.IOException
     * @throws java.net.URISyntaxException
     * @throws net.sf.saxon.trans.XPathException
     * @throws com.processing.exception.XPathEvaluationException
     */
    public final void readGppParametersFromFile(String fileUri) throws IOException, URISyntaxException, XPathException, XPathEvaluationException {
        // We load it using Saxon, as an URIResolver may be needed (ex.: if the URI is a cp:/ one)
        Reader propertiesFileReader = new StandardUnparsedTextResolver().resolve(new URI(fileUri), "UTF-8", saxonConfig);
        Properties properties = new Properties();
        properties.load(propertiesFileReader);
        Map<String, Param> gppParameters = new HashMap<>();
        Iterator it = properties.keySet().iterator();
        while (it.hasNext()) {
            String name = (String) it.next();
            // Parameter value will be evaluated as XPath
            EXpath expath = new EXpath(properties.getProperty(name));
            expath.evaluate(new ArrayList<>());
            gppParameters.put(name, new Param(name, expath.getValue()));
        }
        setGppParameters(gppParameters);
    }
    
    /**
     * Read a Gaulois-Pipe Processing file from a URI and add it to the existing ProcessingStep instance.
     * @param fileUri URI of the Gaulois-Pipe Processing file.
     * @throws ProcessingException If an error occurred reading the file.
     */
    public final void loadGppFile(String fileUri) throws ProcessingException {
        if (gpp != null) {
            gpp.load(fileUri);
        }
        else {
            throw new ProcessingException("Cannot load the given file: no Gaulois-Pipe ProcessingStep has been instantiated yet!");
        }
    }

    /**
     * Add a set of parameters to the Gaulois-Pipe ProcessingStep instance.
     * @param gppParams A set of parameters.
     */
    public final void setGppParameters(Map<String, Param> gppParams) {
        this.gppParams = gppParams;
    }
    
    /**
     * Get the Gaulois-Pipe ProcessingStep instance.
     * @return The Gaulois-Pipe ProcessingStep instance.
     */
    public final ProcessingStep getGppInstance() {
        return gpp;
    }
    
    /**
     * Get the Gaulois-Pipe ProcessingStep instance parameters.
     * @return The Gaulois-Pipe ProcessingStep instance parameters.
     */
    public final Map<String, Param> getGppParameters() {
        return gppParams;
    }

}
