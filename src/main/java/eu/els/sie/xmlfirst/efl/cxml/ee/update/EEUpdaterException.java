package eu.els.sie.xmlfirst.efl.cxml.ee.update;

/**
 * A class for handling EEUpdater exceptions.
 * @author ext-acourt
 */
public class EEUpdaterException extends Exception {

    public EEUpdaterException() {
        super();
    }

    public EEUpdaterException(String message) {
        super(message);
    }

    public EEUpdaterException(String message, Throwable cause) {
        super(message, cause);
    }

    public EEUpdaterException(Throwable cause) {
        super(cause);
    }

    public EEUpdaterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
