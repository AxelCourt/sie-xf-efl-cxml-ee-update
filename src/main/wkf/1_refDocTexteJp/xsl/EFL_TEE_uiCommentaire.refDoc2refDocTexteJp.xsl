<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns:ecm="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm"
                xmlns:eeUp="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/ee-updater"
                xmlns:cx="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
                xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
                xmlns:refDoc="http://www.lefebvre-sarrut.eu/ns/xmlfirst/efl/refDoc"
                xmlns="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
                xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
                exclude-result-prefixes="#all"
                version="3.0">
  
  
  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Conversion des anciens modèles de refDoc vers les refDoc ciblées JP / Texte, pour des EE déjà existantes dans un environnement ECM.</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!-- IMPORTS -->
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  <xsl:import href="xslLib:/xslLib/els-log.xsl"/>
  <xsl:import href="xf-lib:/xf-lib/xsl/_common/xf-common.xsl"/>
  <!-- Pour transformation des refDoc non-résolues -->
  <xsl:import href="xf-sas:/xf-sas/efl/revues_cxml/cxml2flash.modules/contentNode.xsl"/>
  <!-- Pour recalcul des relations -->
  <xsl:import href="xf-lib:/xf-lib/xsl/efl/infoCommentaire.makeRelations.xsl"/>
  
  <!-- Serveur ECM, pour appel au WS de sérialisation des EE -->
  <xsl:param name="ECM.SERVER.URL" select="''" as="xs:string"/>
  <!-- URL du WS de sérialisation sans l'ID de l'EE -->
  <xsl:variable name="eeUrlBegin" select="concat($ECM.SERVER.URL,'ecm-core/api/xmlExporter/exportToXML?flashId=')" as="xs:string"/>
  
  <!-- Pour log -->
  <xsl:variable name="els:currentResName" select="static-base-uri()" as="xs:string"/>
  <!-- Requis par la transformation SAS -->
  <xsl:variable name="xfSAS:company" select="'efl'" as="xs:string"/>
  
  <!-- Modes -->
  <xsl:mode name="eeUp:applyPatch" on-no-match="shallow-copy"/>
  <xsl:mode name="eeUp:patchRefDocElements" on-no-match="shallow-copy"/>
  <xsl:mode name="eeUp:patchRefDocRelations" on-no-match="shallow-copy"/>
  
  <xd:doc>
    <xd:desc>Match par défaut.</xd:desc>
  </xd:doc>
  <xsl:template match="/ | /*">
    <xsl:apply-templates select="." mode="eeUp:applyPatch"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | MODE eeUp:applyPatch                             |
      +==================================================+-->
  
  <xd:doc>
    <xd:desc>
      <xd:p>Correction des refDoc pour compatibilité avec le nouveau format JP / Texte.</xd:p>
      <xd:p>En deux temps : correction des éléments refDoc puis application de la XSL infoCommentaire.makeRelations.xsl (surchargée).</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template match="xf:editorialEntity" mode="eeUp:applyPatch">
    <!-- STEP1 / Correction des éléments refDoc -->
    <xsl:variable name="step" as="element(xf:editorialEntity)">
      <xsl:apply-templates select="." mode="eeUp:patchRefDocElements"/>
    </xsl:variable>
    <!-- STEP2 / Recalcul des relations -->
    <xsl:apply-templates select="$step" mode="xf:makeRelations"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | MODE eeUp:patchRefDocElements                    |
      +==================================================+-->
  
  <xd:doc>
    <xd:desc>Correction de l'élément refDoc.</xd:desc>
  </xd:doc>
  <xsl:template match="cx:refDoc" mode="eeUp:patchRefDocElements">
    <!-- Switch : refDoc résolue ou non -->
    <xsl:choose>
      <xsl:when test="xf:isResolvedLink(.)">
        <!-- refDoc résolue : simple copie + détermination de l'attribut @cible -->
        <xsl:copy copy-namespaces="no">
          <!-- On ne conserve pas les attributs de qualification, ils ne sont de toute façon plus utiles -->
          <xsl:apply-templates select="@* except (@nature, @date, @ref, @rc, @nom)" mode="#current"/>
          <!-- @cible -> besoin d'aller récupérer l'EE associée via le WS pour connaître son code TEE -->
          <xsl:attribute name="cible">
            <xsl:variable name="target.uri" select="concat($eeUrlBegin,key('xf:getRelationById',@xf:idRef)/xf:ref/@xf:targetResId)" as="xs:string"/>
            <xsl:choose>
              <xsl:when test="doc-available($target.uri)">
                <!-- Switch sur code TEE -->
                <xsl:value-of select="if (doc($target.uri)/xf:editorialEntity/@code = 'ELS_TEE_jurisprudence') then 'jp' else 'texte'"/>
              </xsl:when>
              <xsl:otherwise>
                <!-- Erreur + valeur "jp" par défaut -->
                <xsl:call-template name="els:log">
                  <xsl:with-param name="xsltName" select="$els:currentResName" as="xs:string"/>
                  <xsl:with-param name="level" select="'error'" as="xs:string"/>
                  <xsl:with-param name="code" select="'ECM.EE.FOUND'" as="xs:string"/>
                  <xsl:with-param name="markup" select="false()"/>
                  <xsl:with-param name="description" as="xs:string*">EE source non-trouvée : <xsl:value-of select="key('xf:getRelationById',@xf:idRef)/xf:ref/@xf:targetResId"/>.</xsl:with-param>
                </xsl:call-template>
                <xsl:text>jp</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:apply-templates select="node()" mode="#current"/>
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <!-- refDoc non-résolue : on la transforme en refDocReprise et on applique la transformation du SAS sur l'élément
             Ainsi on se retrouvera avec une refDoc conforme au nouveau modèle -->
        <xsl:variable name="refDocReprise" as="element(cx:refDocReprise)">
          <refDocReprise>
            <xsl:if test="not(@nature)">
              <!-- Pour identification dans le bon groupe refDocJp / refDocTexte si @nature n'est pas précisé
                   On récupère "naïvement" le "1er mot" de la verbalisation qu'on confronte à la liste des juridictions
                   (par défaut une refDoc sans @nature est classée comme "refDocTexte") -->
              <xsl:variable name="refDoc:JUR" select="refDoc:getMatchingMap($refDoc:JUR-mapping.cx,normalize-space(replace(text()[1],'^([^\s,]+).+?$','$1')))" as="element()?"/>
              <xsl:if test="exists($refDoc:JUR)">
                <xsl:attribute name="nature" select="$refDoc:JUR/@targetId"/>
              </xsl:if>
            </xsl:if>
            <xsl:apply-templates select="@* | node()" mode="#current"/>
          </refDocReprise>
        </xsl:variable>
        <xsl:apply-templates select="$refDocReprise" mode="xfSAS:revueChaineXml2editorialEntity.contentNode.common-2"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>Normalisation de l'attribut @nature de refDoc pour supprimer les éventuels "e" exposants (posent problème pour la détection de la juridiction par la transformation SAS).</xd:desc>
  </xd:doc>
  <xsl:template match="cx:refDoc/@nature[matches(.,'&lt;sup&gt;e&lt;/sup&gt;')]" mode="eeUp:patchRefDocElements">
    <!-- Ex. : "Cass. 1<sup>e</sup> civ." -> "Cass. 1e civ." -->
    <xsl:attribute name="{name()}" select="replace(.,'&lt;sup&gt;e&lt;/sup&gt;','e')"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | MODES xf:makeRelations + xf:makeRelationElement  |
      +==================================================+-->
  
  <xd:doc>
    <xd:desc>Override xf:makeRelationElement / ne recalcule que les relations des refDoc.</xd:desc>
  </xd:doc>
  <xsl:template match="xf:contentNode" mode="xf:makeRelationElement">
    <!-- Seules les relations des refDoc sont recalculées -->
    <xsl:variable name="original.relations" select="ancestor::xf:editorialEntity/xf:relations/xf:relation" as="element(xf:relation)*"/>
    <xsl:apply-templates select=".//cx:refDoc" mode="#current">
      <xsl:with-param name="original.relations" select="$original.relations"/>
    </xsl:apply-templates>
    <xsl:copy-of select="$original.relations[@xf:id = current()//*[self::cx:renvoiAutrui or self::cx:rMemN or self::cx:rEflN or self::cx:rPratN]/@xf:idRef]" copy-namespaces="no"/>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>Override xf:makeRelationElement / pour conserver l'ID d'origine de la relation.</xd:desc>
    <xd:param name="original.relations">[element(xf:relation)*] Les relations d'origine de l'EE avant recalcul.</xd:param>
  </xd:doc>
  <xsl:template match="cx:refDoc" mode="xf:makeRelationElement">
    <xsl:param name="original.relations" as="element(xf:relation)*"/>
    <!-- Ancienne relation -->
    <xsl:variable name="old.relation" select="$original.relations[@xf:id = current()/@xf:idRef]" as="element(xf:relation)?"/>
    <!-- Nouvelle relation -->
    <xsl:variable name="new.relation" as="element(xf:relation)">
      <xsl:next-match/>
    </xsl:variable>
    <!-- On copie l'ancienne relation, mais on récupère le code et le @xf:targetResId de la nouvelle
           -> sauf si la refDoc est résolue, auquel cas on conserve le @xf:targetResId -->
    <xsl:apply-templates select="$old.relation" mode="eeUp:patchRefDocRelations">
      <xsl:with-param name="new.relation" select="$new.relation" tunnel="yes"/>
      <xsl:with-param name="isResolvedLink" select="not(xf:isQueryRef($old.relation/xf:ref/@xf:targetResId))" tunnel="yes"/>
    </xsl:apply-templates>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>Override xf:makeRelations / ne modifie pas les liens vers les relations existantes.</xd:desc>
  </xd:doc>
  <xsl:template match="cx:refDoc | cx:renvoiAutrui | cx:rMemN | cx:rEflN | cx:rPratN" mode="xf:makeRelations">
    <xsl:copy-of select="." copy-namespaces="no"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | MODE eeUp:patchRefDocRelations                   |
      +==================================================+-->
  
  <xd:doc>
    <xd:desc>Change le code du TIR de la relation.</xd:desc>
    <xd:param name="new.relation">[element(xf:relation)] La nouvelle relation calculée.</xd:param>
  </xd:doc>
  <xsl:template match="xf:relation/@code" mode="eeUp:patchRefDocRelations">
    <xsl:param name="new.relation" as="element(xf:relation)" tunnel="yes"/>
    <xsl:copy-of select="$new.relation/@code" copy-namespaces="no"/>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>Change la cible de la relation.</xd:desc>
    <xd:param name="new.relation">[element(xf:relation)] La nouvelle relation calculée.</xd:param>
    <xd:param name="isResolvedLink">[xs:boolean] Indique si le lien est résolu (auquel cas on conserve l'ancienne cible).</xd:param>
  </xd:doc>
  <xsl:template match="xf:relation/xf:ref/@xf:targetResId" mode="eeUp:patchRefDocRelations">
    <xsl:param name="new.relation" as="element(xf:relation)" tunnel="yes"/>
    <xsl:param name="isResolvedLink" as="xs:boolean" tunnel="yes"/>
    <xsl:copy-of select="if ($isResolvedLink) then (.) else ($new.relation/xf:ref/@xf:targetResId)" copy-namespaces="no"/>
  </xsl:template>
  
</xsl:stylesheet>